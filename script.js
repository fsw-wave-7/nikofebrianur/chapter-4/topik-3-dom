function changeTitle(){
    var title = document.getElementById("title");
    console.log(title);
    title.innerHTML = "SELAMAT MALAM!"
}

function submit(){
    var nama = document.getElementsByName("nama")[0];
    console.log(nama.value)

    var result = document.getElementById("result")
    result.innerHTML = `Selamat Datang, ${nama.value}`
}

/// cara cek array
console.log(document.getElementsByClassName('description'))

const description = document.getElementsByClassName("description")[0];
description.style.color = 'white'
description.style.backgroundColor = 'red'

const description2 = document.getElementsByClassName("description")[1];
description2.style.color = 'white'
description2.style.backgroundColor = 'blue'

//// bikin element baru
const subtitle = document.createElement('p')
subtitle.textContent = 'Ini adalah subtitle dibuat pake JS doang'

document.body.append(subtitle)

////menghapus element
const removed = document.getElementsByClassName('element-dihapus')[0]
removed.remove()

///jQuery
const desc = document.querySelector(".description")
desc.style.color = "gray"

const desc2 = document.querySelectorAll(".description")[1]
desc2.innerHTML = 'Teks ini diubah pake querySelectorAll'