class Card {
    constructor(cardId) {
        this.cardId = cardId
        this.options = [
            {
                name: 'dog',
                url: 'https://image.freepik.com/free-vector/cute-dog-sticking-her-tongue-out-cartoon-icon-illustration_138676-2709.jpg'
            },
            {
                name: 'bear',
                url: 'https://image.freepik.com/free-vector/cute-teddy-bear-waving-hand-cartoon-icon-illustration_138676-2714.jpg'
            },
            {
                name: 'cat',
                url: 'https://image.freepik.com/free-vector/cute-cat-with-cat-bowl-cartoon-icon-illustration_138676-2707.jpg'
            }
        ]
    }

    getCard() {
        const t = this
        return this.options.forEach((opt) => {
            const img = document.createElement('img')
            img.src = opt.url
            img.className = 'm-3'
            img.id = `card-${opt.name}`
            //img.onclick = function() {
            //    t.choosePic(opt.name)
            //}
            document.getElementById(this.cardId).append(img)
        })
    }

    getOptions() {
        return this.options
    }

    choosePic(animal){
        document.getElementsByClassName('choose-result')[0].innerHTML = `I chose ${animal}`
    
    }
}

class RandomCard extends Card {
    constructor(cardId) {
        super(cardId)
    }

    random() {
    let index = Math.floor(Math.random() * Math.floor(3))
    const options = super.getOptions()
    return options[index]
    }
}

const card = new Card('cards')
card.getCard()
const cardComp = new RandomCard(`cards-comp`)
cardComp.getCard()

function getResult(player, comp) {
    let text = ''
    if (player.name == comp.name) {
        text = `Player and Comp choose same card: ${player.name}`
    } else {
        text = `Player and Comp choose different card. Player: ${player.name} and Comp: ${comp.name}`
    }

    document.getElementsByClassName('result')[0].innerHTML = text
}

const options = card.getOptions()
options.forEach((opt) => {
    document.getElementById(`card-${opt.name}`).addEventListener(`click`, () => {
        const player = opt
        const comp = cardComp.random()
        getResult(player, comp)
    })
})

